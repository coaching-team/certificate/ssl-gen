#/bin/bash
# Please refer to GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>

echo "# # # # # # # # # # # #"
echo "# SERVER CA GENERATOR #"
echo "# # # # # # # # # # # #"
echo ""

## Help
if [ "$1" = "--help" ]; then
  echo -e "\nusage: ./serverCA.sh <option>\n"
  echo "  <option>"
  echo "    --help     this manual"
  echo "    --generate <certificate name> <expiration time in days> <root certificate location> <root privateKey keylocation> <config file location>"
  exit 0
fi

## Generate
if [ "$1" = "--generate" ]; then
  
  if [ "$6" = "" ]; then
    echo "less argument then expected"
    echo "please type './serverCA.sh --help' to see usage."
    exit 1
  fi

# ***************************************************************************************** #    
  ## Create Directories
  mkdir $2
  mkdir $2/cert $2/private $2/csr

  ## Generate private key
  echo "***** GENERATE PRIVATE KEY *****"
  openssl genrsa -aes256 -out $2/private/$2Key.pem 2048
  chmod 600 $2/private/$2Key.pem

  ## Generate certificate
  echo "***** GENERATE CERTIFICATE *****"
  openssl req -config $6 -key $2/private/$2Key.pem -new -sha256 -out $2/csr/$2Csr.pem
  
  ## Sign certificate
  echo "***** SIGN CERTIFICATE *****"
  openssl x509 -req -extfile $6 -extensions $2_cert -days $3 -CA $4 -CAkey $5 -CAcreateserial -in $2/csr/$2Csr.pem -out $2/cert/$2Cert.crt
  chmod 640 $2/cert/$2Cert.crt
  
  ## Chech root chain certificate
  echo "***** CHECK ROOT CHAIN CERTIFICATE *****"
  openssl verify -CAfile $4 $2/cert/$2Cert.crt

# ***************************************************************************************** #

else
  echo "Unknown argument: $1"
  echo "please type './serverCA.sh --help' to see usage."
fi
