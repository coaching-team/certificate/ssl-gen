# Description of certification

## usage

Make bash scripts executable:

```
chmod u+x ssl-generate-3-dim.sh
chmod u+x rootCA.sh
chmod u+x intermediateCA.sh
chmod u+x serverCA.sh
```
Execute main bash script:

```
./ssl-generate-3-dim.sh
```

## dimensioned certificates

```
ROOT: SVS-Root-CA
  Intermediate: Sub-CA-10
    Server: svs10.ful.informatik.haw-hamburg.de
```

## License

Serkan Selcuk (c)

This script is licensed under GPLv3

<https://www.gnu.org/licenses/gpl-3.0.en.html>
