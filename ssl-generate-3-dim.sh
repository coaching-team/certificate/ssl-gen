./rootCA.sh --generate root 3650 openssl.cnf
./intermediateCA.sh --generate sub-ca-10 28 root/cert/rootCert.crt root/private/rootKey.pem openssl.cnf
./serverCA.sh --generate server 28 sub-ca-10/cert/root-chain-cert.crt sub-ca-10/private/sub-ca-10Key.pem openssl.cnf

echo "********** ROOT CA **********"
openssl x509 -in root/cert/rootCert.crt -text -noout
echo "********** SUB-CA-10 **********"
openssl x509 -in sub-ca-10/cert/root-chain-cert.crt -text -noout
echo "********** SERVER **********"
openssl x509 -in server/cert/serverCert.crt -text -noout

