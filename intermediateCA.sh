#/bin/bash
# Please refer to GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>

echo "# # # # # # # # # # # # # # #"
echo "# INTERMEDIATE CA GENERATOR #"
echo "# # # # # # # # # # # # # # #"
echo ""

## Help
if [ "$1" = "--help" ]; then
  echo -e "\nusage: ./intermediateCA.sh <option>\n"
  echo "  <option>"
  echo "    --help     this manual"
  echo "    --generate <certificate name> <expiration time in days> <root certificate location> <root privateKey keylocation> <config file location>"
  exit 0
fi

## Generate
if [ "$1" = "--generate" ]; then
  
  if [ "$6" = "" ]; then
    echo "less argument then expected"
    echo "please type './intermediateCA.sh --help' to see usage."
    exit 1
  fi

# ***************************************************************************************** #    
  ## Create Directories
  mkdir $2
  mkdir $2/cert $2/private $2/csr

  ## Generate private key
  echo "***** GENERATE PRIVATE KEY *****"
  openssl genrsa -aes256 -out $2/private/$2Key.pem 4096
  chmod 600 $2/private/$2Key.pem

  ## Generate certificate
  echo "***** GENERATE CERTIFICATE *****"
  openssl req -config $6 -new -sha256 -key $2/private/$2Key.pem -out $2/csr/$2Csr.pem
  
  ## Signed certificate
  echo "***** SIGN CERTIFICATE *****"
  openssl x509 -req -extfile $6 -extensions v3_intermediate_ca -CA $4 -CAkey $5 -CAcreateserial -days $3 -in $2/csr/$2Csr.pem -out $2/cert/$2Cert.crt
  
  ## Generate root chain certificate (Merge certificates)
  echo "***** GENERATE ROOT CHAIN CERTIFICATE *****"
  cat $2/cert/$2Cert.crt $4 > $2/cert/root-chain-cert.crt
  
  ## Chech root chain certificate
  echo "***** CHECK ROOT CHAIN CERTIFICATE *****"
  openssl verify -CAfile $4 $2/cert/root-chain-cert.crt

# ***************************************************************************************** #

else
  echo "Unknown argument: $1"
  echo "please type './intermediateCA.sh --help' to see usage."
fi
