#/bin/bash
# Please refer to GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>

echo "# # # # # # # # # # #"
echo "# ROOT CA GENERATOR #"
echo "# # # # # # # # # # #"
echo ""

## Help
if [ "$1" = "--help" ]; then
  echo -e "\nusage: ./rootCA.sh <option>\n"
  echo "  <option>"
  echo "    --help     this manual"
  echo "    --generate <certificate name> <expiration time in days> <config file location>"
  exit 0
fi

## Generate
if [ "$1" = "--generate" ]; then
  
  if [ "$4" = "" ]; then
    echo "less argument then expected"
    echo "please type './rootCA.sh --help' to see usage."
    exit 1
  fi

# ***************************************************************************************** #    
  ## Create Directories
  mkdir $2
  mkdir $2/cert $2/private

  ## Generate private key
  echo "GENERATE PRIVATE KEY"
  openssl genrsa -aes256 -out $2/private/$2Key.pem 4096
  chmod 600 $2/private/$2Key.pem

  ## Generate certificate
  openssl req -config $4 -key $2/private/$2Key.pem -new -x509 -sha256 -extensions v3_ca -out $2/cert/$2Cert.crt -days $3

# ***************************************************************************************** #

else
  echo "Unknown argument: $1"
  echo "please type './rootCA.sh --help' to see usage."
fi
